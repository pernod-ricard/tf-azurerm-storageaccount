
locals {
  storage_name = replace(var.storage_name == "" ? var.project_name : var.storage_name, " ", "")
  storage_name_cut = substr(local.storage_name, 0, 24 - 3 - length(var.project_entity) - length(var.environment))
}

resource "azurerm_storage_account" "default" {
  name = lower("sto${var.project_entity}${local.storage_name_cut}${var.environment}")
  resource_group_name = var.resourcegroup_name
  location = var.azure_region
  account_tier = "Standard"
  account_replication_type = "LRS"
  account_kind = "StorageV2"
  
  lifecycle {
    ignore_changes = [tags]
  }
}